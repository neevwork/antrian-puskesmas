﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSambutan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblNama = New System.Windows.Forms.Label()
        Me.lblNamaPuskesmas = New System.Windows.Forms.Label()
        Me.btnAction = New System.Windows.Forms.Button()
        Me.picWoman = New System.Windows.Forms.PictureBox()
        Me.picClose = New System.Windows.Forms.PictureBox()
        Me.picLogo = New System.Windows.Forms.PictureBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.picWoman, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picClose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblNama
        '
        Me.lblNama.Font = New System.Drawing.Font("Arial", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNama.Location = New System.Drawing.Point(41, 548)
        Me.lblNama.Name = "lblNama"
        Me.lblNama.Size = New System.Drawing.Size(1184, 58)
        Me.lblNama.TabIndex = 4
        Me.lblNama.Text = "Halo, Hanif"
        Me.lblNama.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNamaPuskesmas
        '
        Me.lblNamaPuskesmas.BackColor = System.Drawing.Color.DarkOrange
        Me.lblNamaPuskesmas.Font = New System.Drawing.Font("Arial", 56.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNamaPuskesmas.ForeColor = System.Drawing.Color.White
        Me.lblNamaPuskesmas.Location = New System.Drawing.Point(35, 621)
        Me.lblNamaPuskesmas.Name = "lblNamaPuskesmas"
        Me.lblNamaPuskesmas.Size = New System.Drawing.Size(1190, 93)
        Me.lblNamaPuskesmas.TabIndex = 5
        Me.lblNamaPuskesmas.Text = "Silahkan Ambil Kartu Anda"
        Me.lblNamaPuskesmas.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnAction
        '
        Me.btnAction.BackColor = System.Drawing.Color.OrangeRed
        Me.btnAction.Font = New System.Drawing.Font("Arial", 39.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAction.ForeColor = System.Drawing.Color.White
        Me.btnAction.Location = New System.Drawing.Point(408, 795)
        Me.btnAction.Name = "btnAction"
        Me.btnAction.Size = New System.Drawing.Size(570, 178)
        Me.btnAction.TabIndex = 6
        Me.btnAction.Text = "YUK, BUAT" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "NO. ANTRIANMU"
        Me.btnAction.UseVisualStyleBackColor = False
        '
        'picWoman
        '
        Me.picWoman.Image = Global.Aplikasi_Antrian_Pasien.My.Resources.Resources.receptionist
        Me.picWoman.Location = New System.Drawing.Point(525, 172)
        Me.picWoman.Name = "picWoman"
        Me.picWoman.Size = New System.Drawing.Size(237, 324)
        Me.picWoman.TabIndex = 3
        Me.picWoman.TabStop = False
        '
        'picClose
        '
        Me.picClose.Image = Global.Aplikasi_Antrian_Pasien.My.Resources.Resources._exit
        Me.picClose.Location = New System.Drawing.Point(1198, 12)
        Me.picClose.Name = "picClose"
        Me.picClose.Size = New System.Drawing.Size(54, 54)
        Me.picClose.TabIndex = 2
        Me.picClose.TabStop = False
        '
        'picLogo
        '
        Me.picLogo.Location = New System.Drawing.Point(25, 21)
        Me.picLogo.Name = "picLogo"
        Me.picLogo.Size = New System.Drawing.Size(158, 134)
        Me.picLogo.TabIndex = 1
        Me.picLogo.TabStop = False
        '
        'Timer1
        '
        Me.Timer1.Interval = 700
        '
        'Timer2
        '
        Me.Timer2.Interval = 1000
        '
        'frmSambutan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1264, 985)
        Me.Controls.Add(Me.btnAction)
        Me.Controls.Add(Me.lblNamaPuskesmas)
        Me.Controls.Add(Me.lblNama)
        Me.Controls.Add(Me.picWoman)
        Me.Controls.Add(Me.picClose)
        Me.Controls.Add(Me.picLogo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmSambutan"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Welcome"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.picWoman, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picClose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picLogo As System.Windows.Forms.PictureBox
    Friend WithEvents picClose As System.Windows.Forms.PictureBox
    Friend WithEvents picWoman As System.Windows.Forms.PictureBox
    Friend WithEvents lblNama As System.Windows.Forms.Label
    Friend WithEvents lblNamaPuskesmas As System.Windows.Forms.Label
    Friend WithEvents btnAction As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
End Class
