﻿Imports System.Drawing.Printing

Public Class clsPrinter

    Dim printDoc As New PrintDocument
    'Dim printerName As String = My.Settings.PrinterName
    Dim TextToPrint As String = ""
    'Dim itemCount As Integer = 0
    Dim BarcodeToPrint, PoliName As String

    Public Sub New()
        AddHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage
        printDoc.PrinterSettings.PrinterName = My.Settings.PrinterName
    End Sub
    Dim LineLen, _PaperLen As Integer
    Public Sub PrintHeader(ByVal namaInstansi As String,
                           ByVal alamatInstansi As String,
                           ByVal judulKarcis As String,
                           ByVal kategori As String,
                           ByVal poli As String,
                           ByVal nopol As String,
                           ByVal currDate As DateTime,
                           ByVal PaperLen As Integer)
        'StrLength = jumlah huruf maksimal yang bisa dicetak dalam satu baris sesuai dengan ukuran lebar kertas roll.
        'send line
        Dim StringToPrint As String
        Dim spcLen As String
        _PaperLen = PaperLen

        StringToPrint = New String("=", PaperLen)
        LineLen = StringToPrint.Length
        spcLen = New String(" "c, Math.Round((PaperLen - LineLen) / 2))
        TextToPrint = spcLen & StringToPrint & Environment.NewLine

        'send Business Name
        StringToPrint = namaInstansi
        LineLen = StringToPrint.Length
        Dim spcLen1 As New String(" "c, Math.Round((PaperLen - LineLen) / 2)) 'This line is used to center text in the middle of the receipt
        'Dim spcLen1 As New String(" "c, 9) 'This line is used to center text in the middle of the receipt
        TextToPrint &= spcLen1 & StringToPrint & Environment.NewLine

        ''send address name
        StringToPrint = alamatInstansi
        LineLen = StringToPrint.Length
        Dim spcLen2 As New String(" "c, Math.Round((PaperLen - LineLen) / 2))
        TextToPrint &= spcLen2 & StringToPrint & Environment.NewLine

        'send line
        StringToPrint = New String("=", PaperLen)
        LineLen = StringToPrint.Length
        Dim spcLen4b As New String(" "c, Math.Round((PaperLen - LineLen) / 2))
        TextToPrint &= spcLen4b & StringToPrint & Environment.NewLine & Environment.NewLine

        ' send Judul Karcis
        StringToPrint = String.Format("{0} {1}", judulKarcis, kategori)
        LineLen = StringToPrint.Length
        spcLen2 = New String(" "c, Math.Round((PaperLen - LineLen) / 2))
        TextToPrint &= spcLen2 & StringToPrint & Environment.NewLine & Environment.NewLine

        'TextToPrint &= "No Antrian: " & nopol & Environment.NewLine
        BarcodeToPrint = nopol
        TextToPrint &= Environment.NewLine & Environment.NewLine & Environment.NewLine & _
            Environment.NewLine & Environment.NewLine & Environment.NewLine & _
            Environment.NewLine & Environment.NewLine & Environment.NewLine
        TextToPrint &= "Tgl Masuk : " & Format(currDate, "dd-MM-yy HH:mm") & Environment.NewLine & Environment.NewLine
        PoliName = poli
        'TextToPrint &= "Poliklinik: " & poli & Environment.NewLine & Environment.NewLine

        'send line
        StringToPrint = New String("=", PaperLen)
        LineLen = StringToPrint.Length
        spcLen4b = New String(" "c, Math.Round((PaperLen - LineLen) / 2))
        TextToPrint &= spcLen4b & StringToPrint & Environment.NewLine

        StringToPrint = "TERIMA KASIH"
        LineLen = StringToPrint.Length
        spcLen2 = New String(" "c, Math.Round((PaperLen - LineLen) / 2))
        TextToPrint &= spcLen2 & StringToPrint & Environment.NewLine
    End Sub
    Private Sub PrintDoc_PrintPage(ByVal sender As Object, ByVal e As Printing.PrintPageEventArgs)
        Static currentChar As Integer
        Dim textfont As Font = New Font("Lucida Console", 8, FontStyle.Regular)

        Dim h, w As Integer
        Dim left, top As Integer
        With printDoc.DefaultPageSettings
            h = 0
            w = 0
            left = 0
            top = 0
        End With

        Dim lines As Integer = CInt(Math.Round(h / 1))
        Dim b As New Rectangle(left, top, w, h)
        Dim format As StringFormat
        format = New StringFormat(StringFormatFlags.LineLimit)
        Dim line, chars As Integer

        e.Graphics.MeasureString(Mid(TextToPrint, currentChar + 1), textfont, New SizeF(w, h), format, chars, line)
        e.Graphics.DrawString(TextToPrint.Substring(currentChar, chars), New Font("Lucida Console", 8, FontStyle.Regular), Brushes.Black, b, format)

        'Rectangle({alignment}, {valignment}, {lebar gambar}, {tinggi gambar})
        e.Graphics.DrawString(BarcodeToPrint, New Font("Helvetica", 28, FontStyle.Bold), Brushes.Black, New Rectangle((_PaperLen * 3) / 2, 80, 200, 200), format)
        Dim teks As String = String.Format("POLI {0}", PoliName)
        'Dim teks As String = String.Format("12345678901234567890", PoliName)

        ' 12 spasi = 6 huruf
        ' satu karakter lebar 12
        Dim PaperLen As Integer = 15
        Dim spasi As New String(" "c, Math.Round((PaperLen - teks.Length) / 2) * 2)
        e.Graphics.DrawString(spasi & teks, New Font("Helvetica", 16, FontStyle.Regular), Brushes.Black, New Rectangle(0, 125, 400, 50), format)

        currentChar = currentChar + chars
        If currentChar < TextToPrint.Length Then
            e.HasMorePages = True
        Else
            e.HasMorePages = False
            currentChar = 0
        End If
    End Sub
    Public Sub Print()
        Dim printControl = New Printing.StandardPrintController
        printDoc.PrintController = printControl

        'MsgBox(printDoc.DefaultPageSettings.PaperSize.Height)
        'printDoc.DefaultPageSettings.PaperSize.Height = 500

        Try
            printDoc.Print()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class
