﻿Imports Newtonsoft.Json.Linq

Public Class frmSambutan
    ' Variable object dataset
    Dim _ds As ds

    ' Varibale untuk menampung nilai pasienID dari form lain
    Dim _pasienID As String

    ' Variable waktu iddle aktivitas pasien
    Dim iddleTime As Integer = 0

    Sub New(ByVal ds As ds, ByVal pasienID As String)
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Try
            ' Ambil data pasien dari datatable
            ' Sesuai dengan pasienID
            'Dim pasien As DataRow() = ds.M_PASIEN.Select(String.Format("IDPASIEN = '{0}'", pasienID))
            Dim ta As New dsTableAdapters.M_PASIENTableAdapter
            Dim pasien As DataRow() = ta.GetData(pasienID).Select

            _ds = ds
            _pasienID = pasienID
            lblNama.Text = String.Format("Halo, {0}", pasien(0)("NAMAPASIEN"))
            lblNamaPuskesmas.Text = "Silahkan Ambil Kartu Anda"

            ' Set logo path
            Dim logoPath As String = String.Format("{0}\assets\img\logo.png", Application.StartupPath())

            ' Tampilkan logo
            Dim bmp As New Bitmap(logoPath)
            picLogo.ImageLocation = (logoPath)
            picLogo.Height = bmp.Height
            picLogo.Width = bmp.Width
            picLogo.Load()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Sub New", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End Try
    End Sub
    ' Event untuk mengatur lokasi control
    ' Agar lebih dinamis
    Private Sub frmUtama_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        Try
            picClose.Left = Me.Size.Width - 80
            picWoman.Left = (Me.Size.Width / 2) - (picWoman.Width / 2)
            lblNama.Left = (Me.Size.Width / 2) - (lblNama.Width / 2)
            lblNamaPuskesmas.Left = (Me.Size.Width / 2) - (lblNamaPuskesmas.Width / 2)
            btnAction.Left = (Me.Size.Width / 2) - (btnAction.Width / 2)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Form Resize", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub picClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picClose.Click
        Me.Close()
    End Sub
    Private Sub btnAction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAction.Click
        Try
            Me.Close()
            ' Tampilkan form proses
            Dim frm As New frmProses(_ds, _pasienID)
            frm.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Button Action", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub frmSambutan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            ' Start timer untuk tampilkan pesan
            ' Supaya pasien mengambil kartu anggota
            ' Dari mesin antrian
            Timer1.Start()

            ' Atur timer 30 detik otomatis close form
            ' Jika pasien tidak melakukan apa-apa
            iddleTime = 0
            Timer2.Start()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Form Load", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End Try
    End Sub
    ' Event untuk menampilkan pesan
    ' Supaya pasien mengambil kartu anggota
    ' Dari mesin antrian
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If lblNamaPuskesmas.Visible Then
            lblNamaPuskesmas.Visible = False
        Else
            lblNamaPuskesmas.Visible = True
        End If
    End Sub
    ' Event untuk menutup form
    ' Jika user tidak melakukan apa-apa
    ' Selama 30 detik
    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        If iddleTime > 30 Then
            Me.Close()
        Else
            iddleTime += 1
        End If
    End Sub
End Class