﻿Imports Newtonsoft.Json.Linq
Imports Newtonsoft.Json

Public Class frmProses
    ' Variable background gambar wizard stage
    Dim step1Path As String = String.Format("{0}\assets\img\step1.png", Application.StartupPath())
    Dim step2Path As String = String.Format("{0}\assets\img\step2.png", Application.StartupPath())
    Dim step3Path As String = String.Format("{0}\assets\img\step3.png", Application.StartupPath())
    Dim step4Path As String = String.Format("{0}\assets\img\step4.png", Application.StartupPath())

    ' Variable object dataset
    Dim _ds As ds

    ' Variable untuk menampung data dari form lain
    Dim _pasienID As String

    ' Variable untuk kedipan pesan error
    Dim count As Integer = 0

    ' Variable dikirim ke form print
    ' Hasil translate dari variable
    ' Yang disimpan di _jawabanStep1, _jawabanStep2, _jawabanStep3
    Dim poli As String
    Dim tipeAntrian As String
    Dim jenisLayanan As String

    ' Variable waktu iddle aktivitas pasien
    Dim iddleTime As Integer

    Sub New(ByVal ds As ds, ByVal pasienID As String)
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Try
            ' Set logo path
            Dim logoPath As String = String.Format("{0}\assets\img\logo.png", Application.StartupPath())

            ' Tampilkan logo
            Dim bmp As New Bitmap(logoPath)
            picLogo.ImageLocation = (logoPath)
            picLogo.Height = bmp.Height
            picLogo.Width = bmp.Width
            picLogo.Load()

            ' Set image timeline / wizard
            ' Ke gambar step 1
            picTimeline.ImageLocation = step1Path
            picTimeline.Load()

            ' Tampung dataset dan pasienid dari form utama ke variable lokal
            _ds = ds
            _pasienID = pasienID
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Sub New", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End Try
    End Sub

    Private Sub UpdateMasterPoli()
        Dim dt As New DataTable
        Dim configText As String = clsPoliConfig.Load()

        ' Tampung pada object json
        Dim jsonSettings As JObject = JObject.Parse(configText.Replace(Chr(10), Chr(13)))

        Dim token As JToken = jsonSettings.GetValue("PoliConfigurations")

        dt = JsonConvert.DeserializeObject(Of DataTable)(token.ToString())

        Dim buttonControl As Object() = {btnPoli1, btnPoli2, btnPoli3}

        For Each row In _ds.M_POLI
            Dim dr() As DataRow = Nothing
            Try
                dr = dt.Select(String.Format("IDPOLI='{0}'", row.IDPOLI))
            Catch ex As Exception
            End Try
            Dim display As String = ""
            Try
                display = dr(0)("DISPLAY")
            Catch ex As Exception
            End Try
            Dim prefix As String = ""
            Try
                prefix = dr(0)("PREFIX")
            Catch ex As Exception
            End Try
            Dim button As String = ""
            Try
                button = dr(0)("CONTROLBUTTON")
            Catch ex As Exception
            End Try

            row("ALIAS") = display
            row("PREFIXANTRIAN") = prefix

            For Each btn In buttonControl
                If btn.Name.ToString() = button Then
                    btn.Tag = row.IDPOLI
                    Exit For
                End If
            Next
        Next
        _ds.M_POLI.AcceptChanges()
    End Sub

    Private Sub frmProcess_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            ' Sesuai dengan kesepakatan terakhir
            ' Agar pasien tidak memilih BPJS / Umum
            ' Dan langsung memilih Poli yang akan dituju
            GotoStep(2)

            UpdateMasterPoli()
            
            ' Set default value untuk variable jawaban
            '_jawabanStep1 = JawabanStep1.NONE
            'ReloadLayoutStep1()
            _jawabanStep2 = Nothing
            'ReloadLayoutStep2()
            _jawabanStep3 = JawabanStep3.NONE
            ReloadLayoutStep3()

            ' Atur timer 30 detik otomatis close form
            ' Jika pasien tidak melakukan apa-apa
            iddleTime = 0
            Timer2.Start()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Form Load", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End Try
    End Sub

    ' Event untuk mengatur lokasi control
    ' Agar lebih dinamis
    Private Sub frmUtama_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        Try
            picTimeline.Left = (Me.Size.Width / 2) - (picTimeline.Width / 2)
            picTimeline.Left = Me.Size.Width - 380

            pnlStep1.Left = (Me.Size.Width / 2) - 630
            pnlStep2.Left = (Me.Size.Width / 2) - 630
            pnlStep3.Left = (Me.Size.Width / 2) - 630
            pnlStep4.Left = (Me.Size.Width / 2) - 630

            lblError.Left = (Me.Size.Width / 2) - (lblError.Width / 2)
            lblInstruksi.Left = (Me.Size.Width / 2) - (lblInstruksi.Width / 2)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Form Resize", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    ' Event untuk menampilkan pesan error
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If count < 7 Then
            If lblError.Visible Then
                lblError.Visible = False
            Else
                lblError.Visible = True
            End If
            count += 1
        Else
            Timer1.Stop()
        End If
    End Sub

    ' Event atur layout wizard
    Private Sub GotoStep(ByVal stepNo As Integer)
        Select Case stepNo
            Case 1
                iddleTime = 0
                picTimeline.ImageLocation = step1Path
                pnlStep1.BringToFront()
            Case 2
                iddleTime = 0
                picTimeline.ImageLocation = step2Path
                pnlStep2.BringToFront()
            Case 3
                iddleTime = 0
                picTimeline.ImageLocation = step3Path
                pnlStep3.BringToFront()
            Case 4
                iddleTime = 0
                picTimeline.ImageLocation = step4Path
                pnlStep4.BringToFront()
        End Select
        picTimeline.Load()
    End Sub

    ' Event untuk pindah ke step 2
    Private Sub btnStep1Selanjutnya_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStep1Selanjutnya.Click
        Try
            If _jawabanStep1 = JawabanStep1.NONE Then
                count = 0
                lblError.Visible = True
                Timer1.Start()
            Else
                lblError.Visible = False
                Timer1.Stop()
                GotoStep(2)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Button Selanjutnya", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    ' Event untuk pindah ke step 3
    Private Sub btnStep2Selanjutnya_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStep2Selanjutnya.Click
        Try
            If _jawabanStep2 = "" Then
                count = 0
                lblError.Visible = True
                Timer1.Start()
            Else
                lblError.Visible = False
                Timer1.Stop()
                GotoStep(3)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Button Selanjutnya", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    ' Event untuk pindah ke step 4
    Private Sub btnStep3Selanjutnya_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStep3Selanjutnya.Click
        Try
            If _jawabanStep3 = JawabanStep3.NONE Then
                count = 0
                lblError.Visible = True
                Timer1.Start()
            Else
                lblError.Visible = False
                Timer1.Stop()
                Select Case _jawabanStep1
                    Case JawabanStep1.BPJS
                        lblSummaryJenisLayanan.Text = "BPJS"
                        jenisLayanan = "BPJS"
                    Case JawabanStep1.UMUM
                        lblSummaryJenisLayanan.Text = "UMUM"
                        jenisLayanan = "UMUM"
                    Case Else
                        lblSummaryJenisLayanan.Text = "-"
                End Select

                lblSummaryPoli.Text = _jawabanStep2

                'Select Case _jawabanStep2
                '    Case JawabanStep2.KIA
                '        lblSummaryPoli.Text = "KIA"
                '        poli = "A"
                '    Case JawabanStep2.UMUM
                '        lblSummaryPoli.Text = "UMUM"
                '        poli = "B"
                '    Case JawabanStep2.GIGI
                '        lblSummaryPoli.Text = "GIGI"
                '        poli = "C"
                '    Case Else
                '        lblSummaryPoli.Text = "-"
                '        poli = ""
                'End Select

                Select Case _jawabanStep3
                    Case JawabanStep3.IYA
                        lblSummaryPrioritas.Text = "IYA"
                        tipeAntrian = "PRIORITAS"
                    Case JawabanStep3.TIDAK
                        lblSummaryPrioritas.Text = "TIDAK"
                        tipeAntrian = "NORMAL"
                    Case Else
                        lblSummaryPrioritas.Text = "-"
                End Select
                GotoStep(4)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Button Selanjutnya", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    ' Event untuk pindah ke step 1
    Private Sub btnStep2Sebelumnya_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStep2Sebelumnya.Click
        Try
            lblError.Visible = False
            Timer1.Stop()
            GotoStep(1)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Button Sebelumnya", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    ' Event untuk pindah ke step 2
    Private Sub btnStep3Sebelumnya_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStep3Sebelumnya.Click
        Try
            lblError.Visible = False
            Timer1.Stop()
            GotoStep(2)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Button Sebelumnya", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    ' Event untuk pindah ke step 3
    Private Sub btnStep4Sebelumnya_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStep4Sebelumnya.Click
        Try
            GotoStep(3)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Button Sebelumnya", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    ' Event untuk cetak no antrian
    Private Sub btnStep4Selanjutnya_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStep4Selanjutnya.Click
        Try
            Me.Close()
            Dim frm As New frmPrint(poli, _ds, jenisLayanan, tipeAntrian, _pasienID)
            frm.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Button Selanjutnya", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

#Region "Step 1"
    Enum JawabanStep1
        NONE
        UMUM
        BPJS
    End Enum
    Dim _jawabanStep1 As JawabanStep1
    Private Sub ReloadLayoutStep1()
        Select Case _jawabanStep1
            Case JawabanStep1.BPJS
                btnBPJS.BackColor = Color.SeaGreen
                btnBPJS.ForeColor = Color.White

                btnUmum.BackColor = Color.DarkGray
                btnUmum.ForeColor = Color.Black
            Case (JawabanStep1.UMUM)
                btnUmum.BackColor = Color.SeaGreen
                btnUmum.ForeColor = Color.White

                btnBPJS.BackColor = Color.DarkGray
                btnBPJS.ForeColor = Color.Black
            Case Else
                btnUmum.BackColor = Color.DarkGray
                btnUmum.ForeColor = Color.Black

                btnBPJS.BackColor = Color.DarkGray
                btnBPJS.ForeColor = Color.Black
        End Select
    End Sub
    Private Sub btnUmum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUmum.Click
        Try
            If _jawabanStep1 = JawabanStep1.UMUM Then
                _jawabanStep1 = JawabanStep1.NONE
            Else
                _jawabanStep1 = JawabanStep1.UMUM
            End If
            ReloadLayoutStep1()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Button Umum", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub btnBPJS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBPJS.Click
        Try
            If _jawabanStep1 = JawabanStep1.BPJS Then
                _jawabanStep1 = JawabanStep1.NONE
            Else
                _jawabanStep1 = JawabanStep1.BPJS
            End If
            ReloadLayoutStep1()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Button BPJS", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
#End Region

#Region "Step 2"
    Dim _jawabanStep2 As String
    Enum poliActiveStatus
        Active
        Inactive
    End Enum
    Private Sub btnPoli1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPoli1.Click
        Try
            _jawabanStep2 = btnPoli1.Tag
            GotoStep(3)

            'If _jawabanStep2 = JawabanStep2.KIA Then
            '    _jawabanStep2 = JawabanStep2.NONE
            'Else
            '    _jawabanStep2 = JawabanStep2.KIA
            'End If
            'ReloadLayoutStep2()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Button Poli 1", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub btnPoli2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPoli2.Click
        Try
            _jawabanStep2 = btnPoli1.Tag
            GotoStep(3)

            'If _jawabanStep2 = JawabanStep2.UMUM Then
            '    _jawabanStep2 = JawabanStep2.NONE
            'Else
            '    _jawabanStep2 = JawabanStep2.UMUM
            'End If
            'ReloadLayoutStep2()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Button Poli 2", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub btnPoli3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPoli3.Click
        Try
            _jawabanStep2 = btnPoli1.Tag
            GotoStep(3)

            'If _jawabanStep2 = JawabanStep2.GIGI Then
            '    _jawabanStep2 = JawabanStep2.NONE
            'Else
            '    _jawabanStep2 = JawabanStep2.GIGI
            'End If
            'ReloadLayoutStep2()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Button Poli 3", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    '' MULAI - Dimatikan, Jawaban step 2 mengacu ke properti tag
    'Enum JawabanStep2
    '    NONE
    '    KIA
    '    UMUM
    '    GIGI
    'End Enum
    'Dim _jawabanStep2 As JawabanStep2
    'Private Sub SetStatusPoli(ByVal btnPoliIndex As JawabanStep2, ByVal status As poliActiveStatus)
    '    Select Case status
    '        Case poliActiveStatus.Active
    '            Select Case btnPoliIndex
    '                Case JawabanStep2.KIA
    '                    btnPoli1.BackColor = Color.SeaGreen
    '                    btnPoli1.ForeColor = Color.White
    '                Case JawabanStep2.UMUM
    '                    btnPoli2.BackColor = Color.SeaGreen
    '                    btnPoli2.ForeColor = Color.White
    '                Case JawabanStep2.GIGI
    '                    btnPoli3.BackColor = Color.SeaGreen
    '                    btnPoli3.ForeColor = Color.White
    '            End Select
    '        Case Else
    '            Select Case btnPoliIndex
    '                Case JawabanStep2.KIA
    '                    btnPoli1.BackColor = Color.DarkGray
    '                    btnPoli1.ForeColor = Color.Black
    '                Case JawabanStep2.UMUM
    '                    btnPoli2.BackColor = Color.DarkGray
    '                    btnPoli2.ForeColor = Color.Black
    '                Case JawabanStep2.GIGI
    '                    btnPoli3.BackColor = Color.DarkGray
    '                    btnPoli3.ForeColor = Color.Black
    '            End Select
    '    End Select
    'End Sub
    'Private Sub ReloadLayoutStep2()
    '    Select Case _jawabanStep2
    '        Case JawabanStep2.KIA
    '            SetStatusPoli(JawabanStep2.KIA, poliActiveStatus.Active)
    '            SetStatusPoli(JawabanStep2.UMUM, poliActiveStatus.Inactive)
    '            SetStatusPoli(JawabanStep2.GIGI, poliActiveStatus.Inactive)
    '        Case JawabanStep2.UMUM
    '            SetStatusPoli(JawabanStep2.KIA, poliActiveStatus.Inactive)
    '            SetStatusPoli(JawabanStep2.UMUM, poliActiveStatus.Active)
    '            SetStatusPoli(JawabanStep2.GIGI, poliActiveStatus.Inactive)
    '        Case JawabanStep2.GIGI
    '            SetStatusPoli(JawabanStep2.KIA, poliActiveStatus.Inactive)
    '            SetStatusPoli(JawabanStep2.UMUM, poliActiveStatus.Inactive)
    '            SetStatusPoli(JawabanStep2.GIGI, poliActiveStatus.Active)
    '        Case Else
    '            SetStatusPoli(JawabanStep2.KIA, poliActiveStatus.Inactive)
    '            SetStatusPoli(JawabanStep2.UMUM, poliActiveStatus.Inactive)
    '            SetStatusPoli(JawabanStep2.GIGI, poliActiveStatus.Inactive)
    '    End Select
    'End Sub
    '' SELESAI - Dimatikan, Jawaban step 2 mengacu ke properti tag
#End Region

#Region "Step 3"
    Enum JawabanStep3
        NONE
        IYA
        TIDAK
    End Enum
    Dim _jawabanStep3 As JawabanStep3
    Private Sub ReloadLayoutStep3()
        Select Case _jawabanStep3
            Case JawabanStep3.IYA
                btnIya.BackColor = Color.SeaGreen
                btnIya.ForeColor = Color.White

                btnTidak.BackColor = Color.DarkGray
                btnTidak.ForeColor = Color.Black
            Case JawabanStep3.TIDAK
                btnIya.BackColor = Color.DarkGray
                btnIya.ForeColor = Color.Black

                btnTidak.BackColor = Color.SeaGreen
                btnTidak.ForeColor = Color.White
            Case Else
                btnIya.BackColor = Color.DarkGray
                btnIya.ForeColor = Color.Black

                btnTidak.BackColor = Color.DarkGray
                btnTidak.ForeColor = Color.Black
        End Select
    End Sub
    Private Sub btnIya_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIya.Click
        Try
            _jawabanStep3 = JawabanStep3.IYA
            ShowSummary()

            'If _jawabanStep3 = JawabanStep3.IYA Then
            '    _jawabanStep3 = JawabanStep3.NONE
            'Else
            '    _jawabanStep3 = JawabanStep3.IYA
            'End If
            'ReloadLayoutStep3()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Button Iya", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub btnTidak_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTidak.Click
        Try
            _jawabanStep3 = JawabanStep3.TIDAK
            ShowSummary()

            'If _jawabanStep3 = JawabanStep3.TIDAK Then
            '    _jawabanStep3 = JawabanStep3.NONE
            'Else
            '    _jawabanStep3 = JawabanStep3.TIDAK
            'End If
            'ReloadLayoutStep3()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Button Tidak", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ShowSummary()
        Try
            'Dim pasien As DataRow() = _ds.M_PASIEN.Select(String.Format("IDPASIEN = '{0}'", _pasienID))
            Dim ta As New dsTableAdapters.M_PASIENTableAdapter
            Dim pasien As DataRow() = ta.GetData(_pasienID).Select

            If pasien.Length > 0 Then
                lblSummaryNama.Text = pasien(0)("NAMAPASIEN").ToString
                lblSummaryJenisLayanan.Text = pasien(0)("TIPEKARTU").ToString
            Else
                lblSummaryNama.Text = ""
                lblSummaryJenisLayanan.Text = "UMUM"
            End If

            Select Case lblSummaryJenisLayanan.Text
                Case "BPJS"
                    lblInstruksi.Visible = True
                    Timer3.Start()
                Case "UMUM"
                    lblInstruksi.Visible = False
                    Timer3.Stop()
            End Select

            Dim dr() As DataRow = _ds.M_POLI.Select(String.Format("IDPOLI='{0}'", _jawabanStep2))

            lblSummaryPoli.Text = "-"
            poli = "-"

            If dr.Length > 0 Then
                lblSummaryPoli.Text = dr(0)("ALIAS")
                poli = dr(0)("PREFIXANTRIAN")
            End If

            Select Case _jawabanStep3
                Case JawabanStep3.IYA
                    lblSummaryPrioritas.Text = "IYA"
                    tipeAntrian = "PRIORITAS"
                Case JawabanStep3.TIDAK
                    lblSummaryPrioritas.Text = "TIDAK"
                    tipeAntrian = "NORMAL"
                Case Else
                    lblSummaryPrioritas.Text = "-"
            End Select
            GotoStep(4)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Sub Show Summary", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
#End Region

    ' Event untuk menutup form
    ' Jika user tidak melakukan apa-apa
    ' Selama 30 detik
    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        If iddleTime > 30 Then
            Me.Close()
        Else
            iddleTime += 1
        End If
    End Sub

    ' Event instruksi (berkedip) untuk pasien BPJS
    Private Sub Timer3_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer3.Tick
        If lblInstruksi.Visible Then
            lblInstruksi.Visible = False
        Else
            lblInstruksi.Visible = True
        End If
    End Sub
End Class