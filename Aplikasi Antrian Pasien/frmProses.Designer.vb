﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProses
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.picTimeline = New System.Windows.Forms.PictureBox()
        Me.picLogo = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnUmum = New System.Windows.Forms.Button()
        Me.pnlStep1 = New System.Windows.Forms.Panel()
        Me.btnBPJS = New System.Windows.Forms.Button()
        Me.btnStep1Selanjutnya = New System.Windows.Forms.Button()
        Me.pnlStep2 = New System.Windows.Forms.Panel()
        Me.btnPoli3 = New System.Windows.Forms.Button()
        Me.btnPoli2 = New System.Windows.Forms.Button()
        Me.btnStep2Sebelumnya = New System.Windows.Forms.Button()
        Me.btnStep2Selanjutnya = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnPoli1 = New System.Windows.Forms.Button()
        Me.pnlStep3 = New System.Windows.Forms.Panel()
        Me.btnTidak = New System.Windows.Forms.Button()
        Me.btnIya = New System.Windows.Forms.Button()
        Me.btnStep3Sebelumnya = New System.Windows.Forms.Button()
        Me.btnStep3Selanjutnya = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.pnlStep4 = New System.Windows.Forms.Panel()
        Me.lblSummaryNama = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblSummaryPrioritas = New System.Windows.Forms.Label()
        Me.lblSummaryPoli = New System.Windows.Forms.Label()
        Me.lblSummaryJenisLayanan = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnStep4Sebelumnya = New System.Windows.Forms.Button()
        Me.btnStep4Selanjutnya = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblError = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.lblInstruksi = New System.Windows.Forms.Label()
        Me.Timer3 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.picTimeline, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlStep1.SuspendLayout()
        Me.pnlStep2.SuspendLayout()
        Me.pnlStep3.SuspendLayout()
        Me.pnlStep4.SuspendLayout()
        Me.SuspendLayout()
        '
        'picTimeline
        '
        Me.picTimeline.Location = New System.Drawing.Point(906, 156)
        Me.picTimeline.Name = "picTimeline"
        Me.picTimeline.Size = New System.Drawing.Size(332, 143)
        Me.picTimeline.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picTimeline.TabIndex = 3
        Me.picTimeline.TabStop = False
        '
        'picLogo
        '
        Me.picLogo.Location = New System.Drawing.Point(25, 21)
        Me.picLogo.Name = "picLogo"
        Me.picLogo.Size = New System.Drawing.Size(158, 134)
        Me.picLogo.TabIndex = 1
        Me.picLogo.TabStop = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Arial", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 53)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(866, 57)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Mau pilih jenis layanan apa?"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnUmum
        '
        Me.btnUmum.BackColor = System.Drawing.Color.DarkGray
        Me.btnUmum.Font = New System.Drawing.Font("Arial", 39.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUmum.ForeColor = System.Drawing.Color.Black
        Me.btnUmum.Location = New System.Drawing.Point(16, 317)
        Me.btnUmum.Name = "btnUmum"
        Me.btnUmum.Size = New System.Drawing.Size(387, 139)
        Me.btnUmum.TabIndex = 6
        Me.btnUmum.Text = "UMUM"
        Me.btnUmum.UseVisualStyleBackColor = False
        '
        'pnlStep1
        '
        Me.pnlStep1.BackColor = System.Drawing.Color.White
        Me.pnlStep1.Controls.Add(Me.btnBPJS)
        Me.pnlStep1.Controls.Add(Me.btnStep1Selanjutnya)
        Me.pnlStep1.Controls.Add(Me.Label1)
        Me.pnlStep1.Controls.Add(Me.btnUmum)
        Me.pnlStep1.Location = New System.Drawing.Point(25, 159)
        Me.pnlStep1.Name = "pnlStep1"
        Me.pnlStep1.Size = New System.Drawing.Size(875, 763)
        Me.pnlStep1.TabIndex = 7
        '
        'btnBPJS
        '
        Me.btnBPJS.BackColor = System.Drawing.Color.DarkGray
        Me.btnBPJS.Font = New System.Drawing.Font("Arial", 39.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBPJS.ForeColor = System.Drawing.Color.Black
        Me.btnBPJS.Location = New System.Drawing.Point(474, 317)
        Me.btnBPJS.Name = "btnBPJS"
        Me.btnBPJS.Size = New System.Drawing.Size(387, 139)
        Me.btnBPJS.TabIndex = 9
        Me.btnBPJS.Text = "BPJS"
        Me.btnBPJS.UseVisualStyleBackColor = False
        '
        'btnStep1Selanjutnya
        '
        Me.btnStep1Selanjutnya.BackColor = System.Drawing.Color.OrangeRed
        Me.btnStep1Selanjutnya.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStep1Selanjutnya.ForeColor = System.Drawing.Color.White
        Me.btnStep1Selanjutnya.Location = New System.Drawing.Point(613, 679)
        Me.btnStep1Selanjutnya.Name = "btnStep1Selanjutnya"
        Me.btnStep1Selanjutnya.Size = New System.Drawing.Size(248, 64)
        Me.btnStep1Selanjutnya.TabIndex = 7
        Me.btnStep1Selanjutnya.Text = "SELANJUTNYA"
        Me.btnStep1Selanjutnya.UseVisualStyleBackColor = False
        '
        'pnlStep2
        '
        Me.pnlStep2.BackColor = System.Drawing.Color.White
        Me.pnlStep2.Controls.Add(Me.btnPoli3)
        Me.pnlStep2.Controls.Add(Me.btnPoli2)
        Me.pnlStep2.Controls.Add(Me.btnStep2Sebelumnya)
        Me.pnlStep2.Controls.Add(Me.btnStep2Selanjutnya)
        Me.pnlStep2.Controls.Add(Me.Label2)
        Me.pnlStep2.Controls.Add(Me.btnPoli1)
        Me.pnlStep2.Location = New System.Drawing.Point(25, 159)
        Me.pnlStep2.Name = "pnlStep2"
        Me.pnlStep2.Size = New System.Drawing.Size(875, 763)
        Me.pnlStep2.TabIndex = 10
        '
        'btnPoli3
        '
        Me.btnPoli3.BackColor = System.Drawing.Color.DarkGray
        Me.btnPoli3.Font = New System.Drawing.Font("Arial", 39.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPoli3.ForeColor = System.Drawing.Color.Black
        Me.btnPoli3.Location = New System.Drawing.Point(16, 317)
        Me.btnPoli3.Name = "btnPoli3"
        Me.btnPoli3.Size = New System.Drawing.Size(387, 139)
        Me.btnPoli3.TabIndex = 10
        Me.btnPoli3.Tag = ""
        Me.btnPoli3.Text = "GIGI"
        Me.btnPoli3.UseVisualStyleBackColor = False
        '
        'btnPoli2
        '
        Me.btnPoli2.BackColor = System.Drawing.Color.DarkGray
        Me.btnPoli2.Font = New System.Drawing.Font("Arial", 39.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPoli2.ForeColor = System.Drawing.Color.Black
        Me.btnPoli2.Location = New System.Drawing.Point(474, 148)
        Me.btnPoli2.Name = "btnPoli2"
        Me.btnPoli2.Size = New System.Drawing.Size(387, 139)
        Me.btnPoli2.TabIndex = 9
        Me.btnPoli2.Tag = ""
        Me.btnPoli2.Text = "UMUM"
        Me.btnPoli2.UseVisualStyleBackColor = False
        '
        'btnStep2Sebelumnya
        '
        Me.btnStep2Sebelumnya.BackColor = System.Drawing.Color.White
        Me.btnStep2Sebelumnya.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStep2Sebelumnya.ForeColor = System.Drawing.Color.DarkGray
        Me.btnStep2Sebelumnya.Location = New System.Drawing.Point(16, 679)
        Me.btnStep2Sebelumnya.Name = "btnStep2Sebelumnya"
        Me.btnStep2Sebelumnya.Size = New System.Drawing.Size(248, 64)
        Me.btnStep2Sebelumnya.TabIndex = 8
        Me.btnStep2Sebelumnya.Text = "SEBELUMNYA"
        Me.btnStep2Sebelumnya.UseVisualStyleBackColor = False
        Me.btnStep2Sebelumnya.Visible = False
        '
        'btnStep2Selanjutnya
        '
        Me.btnStep2Selanjutnya.BackColor = System.Drawing.Color.OrangeRed
        Me.btnStep2Selanjutnya.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStep2Selanjutnya.ForeColor = System.Drawing.Color.White
        Me.btnStep2Selanjutnya.Location = New System.Drawing.Point(613, 679)
        Me.btnStep2Selanjutnya.Name = "btnStep2Selanjutnya"
        Me.btnStep2Selanjutnya.Size = New System.Drawing.Size(248, 64)
        Me.btnStep2Selanjutnya.TabIndex = 7
        Me.btnStep2Selanjutnya.Text = "SELANJUTNYA"
        Me.btnStep2Selanjutnya.UseVisualStyleBackColor = False
        Me.btnStep2Selanjutnya.Visible = False
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Arial", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(866, 57)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Mau pilih poliklinik yang mana?"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnPoli1
        '
        Me.btnPoli1.BackColor = System.Drawing.Color.DarkGray
        Me.btnPoli1.Font = New System.Drawing.Font("Arial", 39.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPoli1.ForeColor = System.Drawing.Color.Black
        Me.btnPoli1.Location = New System.Drawing.Point(16, 148)
        Me.btnPoli1.Name = "btnPoli1"
        Me.btnPoli1.Size = New System.Drawing.Size(387, 139)
        Me.btnPoli1.TabIndex = 6
        Me.btnPoli1.Tag = ""
        Me.btnPoli1.Text = "KIA"
        Me.btnPoli1.UseVisualStyleBackColor = False
        '
        'pnlStep3
        '
        Me.pnlStep3.BackColor = System.Drawing.Color.White
        Me.pnlStep3.Controls.Add(Me.btnTidak)
        Me.pnlStep3.Controls.Add(Me.btnIya)
        Me.pnlStep3.Controls.Add(Me.btnStep3Sebelumnya)
        Me.pnlStep3.Controls.Add(Me.btnStep3Selanjutnya)
        Me.pnlStep3.Controls.Add(Me.Label3)
        Me.pnlStep3.Location = New System.Drawing.Point(25, 159)
        Me.pnlStep3.Name = "pnlStep3"
        Me.pnlStep3.Size = New System.Drawing.Size(875, 763)
        Me.pnlStep3.TabIndex = 11
        '
        'btnTidak
        '
        Me.btnTidak.BackColor = System.Drawing.Color.DarkGray
        Me.btnTidak.Font = New System.Drawing.Font("Arial", 39.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTidak.ForeColor = System.Drawing.Color.Black
        Me.btnTidak.Location = New System.Drawing.Point(474, 317)
        Me.btnTidak.Name = "btnTidak"
        Me.btnTidak.Size = New System.Drawing.Size(387, 139)
        Me.btnTidak.TabIndex = 11
        Me.btnTidak.Text = "TIDAK"
        Me.btnTidak.UseVisualStyleBackColor = False
        '
        'btnIya
        '
        Me.btnIya.BackColor = System.Drawing.Color.DarkGray
        Me.btnIya.Font = New System.Drawing.Font("Arial", 39.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnIya.ForeColor = System.Drawing.Color.Black
        Me.btnIya.Location = New System.Drawing.Point(16, 317)
        Me.btnIya.Name = "btnIya"
        Me.btnIya.Size = New System.Drawing.Size(387, 139)
        Me.btnIya.TabIndex = 10
        Me.btnIya.Text = "IYA"
        Me.btnIya.UseVisualStyleBackColor = False
        '
        'btnStep3Sebelumnya
        '
        Me.btnStep3Sebelumnya.BackColor = System.Drawing.Color.White
        Me.btnStep3Sebelumnya.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStep3Sebelumnya.ForeColor = System.Drawing.Color.DarkGray
        Me.btnStep3Sebelumnya.Location = New System.Drawing.Point(16, 679)
        Me.btnStep3Sebelumnya.Name = "btnStep3Sebelumnya"
        Me.btnStep3Sebelumnya.Size = New System.Drawing.Size(248, 64)
        Me.btnStep3Sebelumnya.TabIndex = 8
        Me.btnStep3Sebelumnya.Text = "SEBELUMNYA"
        Me.btnStep3Sebelumnya.UseVisualStyleBackColor = False
        '
        'btnStep3Selanjutnya
        '
        Me.btnStep3Selanjutnya.BackColor = System.Drawing.Color.OrangeRed
        Me.btnStep3Selanjutnya.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStep3Selanjutnya.ForeColor = System.Drawing.Color.White
        Me.btnStep3Selanjutnya.Location = New System.Drawing.Point(613, 679)
        Me.btnStep3Selanjutnya.Name = "btnStep3Selanjutnya"
        Me.btnStep3Selanjutnya.Size = New System.Drawing.Size(248, 64)
        Me.btnStep3Selanjutnya.TabIndex = 7
        Me.btnStep3Selanjutnya.Text = "SELANJUTNYA"
        Me.btnStep3Selanjutnya.UseVisualStyleBackColor = False
        Me.btnStep3Selanjutnya.Visible = False
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Arial", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 53)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(866, 57)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Antri sebagai pasien prioritas?"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlStep4
        '
        Me.pnlStep4.BackColor = System.Drawing.Color.White
        Me.pnlStep4.Controls.Add(Me.lblSummaryNama)
        Me.pnlStep4.Controls.Add(Me.Label9)
        Me.pnlStep4.Controls.Add(Me.lblSummaryPrioritas)
        Me.pnlStep4.Controls.Add(Me.lblSummaryPoli)
        Me.pnlStep4.Controls.Add(Me.lblSummaryJenisLayanan)
        Me.pnlStep4.Controls.Add(Me.Label7)
        Me.pnlStep4.Controls.Add(Me.Label6)
        Me.pnlStep4.Controls.Add(Me.Label5)
        Me.pnlStep4.Controls.Add(Me.btnStep4Sebelumnya)
        Me.pnlStep4.Controls.Add(Me.btnStep4Selanjutnya)
        Me.pnlStep4.Controls.Add(Me.Label4)
        Me.pnlStep4.Location = New System.Drawing.Point(25, 159)
        Me.pnlStep4.Name = "pnlStep4"
        Me.pnlStep4.Size = New System.Drawing.Size(875, 763)
        Me.pnlStep4.TabIndex = 12
        '
        'lblSummaryNama
        '
        Me.lblSummaryNama.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSummaryNama.Location = New System.Drawing.Point(3, 173)
        Me.lblSummaryNama.Name = "lblSummaryNama"
        Me.lblSummaryNama.Size = New System.Drawing.Size(866, 78)
        Me.lblSummaryNama.TabIndex = 16
        Me.lblSummaryNama.Text = "-"
        Me.lblSummaryNama.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Arial", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(13, 123)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(851, 38)
        Me.Label9.TabIndex = 15
        Me.Label9.Text = "Nama kamu:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSummaryPrioritas
        '
        Me.lblSummaryPrioritas.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSummaryPrioritas.Location = New System.Drawing.Point(6, 581)
        Me.lblSummaryPrioritas.Name = "lblSummaryPrioritas"
        Me.lblSummaryPrioritas.Size = New System.Drawing.Size(866, 78)
        Me.lblSummaryPrioritas.TabIndex = 14
        Me.lblSummaryPrioritas.Text = "-"
        Me.lblSummaryPrioritas.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSummaryPoli
        '
        Me.lblSummaryPoli.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSummaryPoli.Location = New System.Drawing.Point(6, 447)
        Me.lblSummaryPoli.Name = "lblSummaryPoli"
        Me.lblSummaryPoli.Size = New System.Drawing.Size(866, 78)
        Me.lblSummaryPoli.TabIndex = 13
        Me.lblSummaryPoli.Text = "-"
        Me.lblSummaryPoli.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSummaryJenisLayanan
        '
        Me.lblSummaryJenisLayanan.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSummaryJenisLayanan.Location = New System.Drawing.Point(6, 318)
        Me.lblSummaryJenisLayanan.Name = "lblSummaryJenisLayanan"
        Me.lblSummaryJenisLayanan.Size = New System.Drawing.Size(866, 78)
        Me.lblSummaryJenisLayanan.TabIndex = 12
        Me.lblSummaryJenisLayanan.Text = "-"
        Me.lblSummaryJenisLayanan.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Arial", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(10, 540)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(851, 38)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Sebagai pasien prioritas:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Arial", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(10, 409)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(851, 38)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Poliklinik yang kamu pilih:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Arial", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(10, 270)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(851, 38)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Jenis layanan yang kamu pilih:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnStep4Sebelumnya
        '
        Me.btnStep4Sebelumnya.BackColor = System.Drawing.Color.White
        Me.btnStep4Sebelumnya.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStep4Sebelumnya.ForeColor = System.Drawing.Color.DarkGray
        Me.btnStep4Sebelumnya.Location = New System.Drawing.Point(16, 679)
        Me.btnStep4Sebelumnya.Name = "btnStep4Sebelumnya"
        Me.btnStep4Sebelumnya.Size = New System.Drawing.Size(248, 64)
        Me.btnStep4Sebelumnya.TabIndex = 8
        Me.btnStep4Sebelumnya.Text = "SEBELUMNYA"
        Me.btnStep4Sebelumnya.UseVisualStyleBackColor = False
        '
        'btnStep4Selanjutnya
        '
        Me.btnStep4Selanjutnya.BackColor = System.Drawing.Color.OrangeRed
        Me.btnStep4Selanjutnya.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStep4Selanjutnya.ForeColor = System.Drawing.Color.White
        Me.btnStep4Selanjutnya.Location = New System.Drawing.Point(579, 679)
        Me.btnStep4Selanjutnya.Name = "btnStep4Selanjutnya"
        Me.btnStep4Selanjutnya.Size = New System.Drawing.Size(282, 64)
        Me.btnStep4Selanjutnya.TabIndex = 7
        Me.btnStep4Selanjutnya.Text = "CETAK NO. ANTRIAN"
        Me.btnStep4Selanjutnya.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Arial", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 53)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(866, 57)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Konfirmasi Data"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblError
        '
        Me.lblError.BackColor = System.Drawing.Color.Red
        Me.lblError.Font = New System.Drawing.Font("Arial", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblError.ForeColor = System.Drawing.Color.White
        Me.lblError.Location = New System.Drawing.Point(218, 82)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(854, 58)
        Me.lblError.TabIndex = 13
        Me.lblError.Text = "HARUS PILIH SALAH SATU"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblError.Visible = False
        '
        'Timer1
        '
        Me.Timer1.Interval = 700
        '
        'Timer2
        '
        Me.Timer2.Interval = 1000
        '
        'lblInstruksi
        '
        Me.lblInstruksi.BackColor = System.Drawing.Color.OliveDrab
        Me.lblInstruksi.Font = New System.Drawing.Font("Arial", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstruksi.ForeColor = System.Drawing.Color.White
        Me.lblInstruksi.Location = New System.Drawing.Point(12, 925)
        Me.lblInstruksi.Name = "lblInstruksi"
        Me.lblInstruksi.Size = New System.Drawing.Size(1167, 58)
        Me.lblInstruksi.TabIndex = 14
        Me.lblInstruksi.Text = "SILAHKAN MENUJU LOKET UNTUK KONFIRMASI BPJS"
        Me.lblInstruksi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblInstruksi.Visible = False
        '
        'Timer3
        '
        Me.Timer3.Interval = 700
        '
        'frmProses
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1264, 985)
        Me.Controls.Add(Me.lblInstruksi)
        Me.Controls.Add(Me.lblError)
        Me.Controls.Add(Me.picTimeline)
        Me.Controls.Add(Me.picLogo)
        Me.Controls.Add(Me.pnlStep4)
        Me.Controls.Add(Me.pnlStep2)
        Me.Controls.Add(Me.pnlStep1)
        Me.Controls.Add(Me.pnlStep3)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmProses"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Process"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.picTimeline, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlStep1.ResumeLayout(False)
        Me.pnlStep2.ResumeLayout(False)
        Me.pnlStep3.ResumeLayout(False)
        Me.pnlStep4.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picLogo As System.Windows.Forms.PictureBox
    Friend WithEvents picTimeline As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnUmum As System.Windows.Forms.Button
    Friend WithEvents pnlStep1 As System.Windows.Forms.Panel
    Friend WithEvents btnStep1Selanjutnya As System.Windows.Forms.Button
    Friend WithEvents btnBPJS As System.Windows.Forms.Button
    Friend WithEvents pnlStep2 As System.Windows.Forms.Panel
    Friend WithEvents btnPoli3 As System.Windows.Forms.Button
    Friend WithEvents btnPoli2 As System.Windows.Forms.Button
    Friend WithEvents btnStep2Sebelumnya As System.Windows.Forms.Button
    Friend WithEvents btnStep2Selanjutnya As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnPoli1 As System.Windows.Forms.Button
    Friend WithEvents pnlStep3 As System.Windows.Forms.Panel
    Friend WithEvents btnTidak As System.Windows.Forms.Button
    Friend WithEvents btnIya As System.Windows.Forms.Button
    Friend WithEvents btnStep3Sebelumnya As System.Windows.Forms.Button
    Friend WithEvents btnStep3Selanjutnya As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents pnlStep4 As System.Windows.Forms.Panel
    Friend WithEvents btnStep4Sebelumnya As System.Windows.Forms.Button
    Friend WithEvents btnStep4Selanjutnya As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblSummaryPrioritas As System.Windows.Forms.Label
    Friend WithEvents lblSummaryPoli As System.Windows.Forms.Label
    Friend WithEvents lblSummaryJenisLayanan As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents lblSummaryNama As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents lblInstruksi As System.Windows.Forms.Label
    Friend WithEvents Timer3 As System.Windows.Forms.Timer
End Class
