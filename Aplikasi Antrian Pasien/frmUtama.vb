﻿Imports System.IO
Imports Newtonsoft.Json.Linq

Public Class frmUtama
    ' Variable untuk menampung gambar banner
    Dim listBanner As New ArrayList

    ' Variable object table adapter
    Dim M_PASIENTableAdapters As New dsTableAdapters.M_PASIENTableAdapter
    Dim T_KEDATANGANTableAdapters As New dsTableAdapters.T_KEDATANGANTableAdapter
    Dim M_POLITableAdapters As New dsTableAdapters.M_POLITableAdapter

    ' Variable object dataset
    Dim ds As New ds

    Dim refreshTime As Integer

    Sub New()
        ' This call is required by the designer.
        InitializeComponent()

        Try
            ' Add any initialization after the InitializeComponent() call.

            ' Baca file config
            Dim configText As String
            configText = clsConfig.Load()

            ' Tampung pada object json
            Dim jsonSettings As JObject = JObject.Parse(configText.Replace(Chr(10), Chr(13)))

            ' Simpan nilai file config ke variable property
            Label1.Text = jsonSettings("Instansi")("Nama").ToString
            Label2.Text = jsonSettings("Instansi")("Alamat").ToString
            My.Settings.Item("NamaInstansi") = Label1.Text
            My.Settings.Item("AlamatInstansi") = Label2.Text
            My.Settings.Item("PrinterName") = jsonSettings("PrinterName").ToString
            My.Settings.Item("PaperLen") = jsonSettings("PaperLen").ToString
            My.Settings.Item("LocalPort") = jsonSettings("Local")("Port").ToString
            My.Settings.Item("ConnectionString") = String.Format("Data Source={0};Initial Catalog={1};Persist Security Info=False;User ID=sa;Password=S3cr3t", jsonSettings("DB")("Host").ToString, jsonSettings("DB")("Name").ToString)

            ' Atur lokasi control picture untuk background
            picScreen.Top = picBackground.Location.Y + 45

            ' Set logo path
            Dim logoPath As String = String.Format("{0}\assets\img\logo.png", Application.StartupPath())

            ' Tampilkan logo
            Dim bmp As New Bitmap(logoPath)
            picLogo.Height = bmp.Height
            picLogo.ImageLocation = (logoPath)
            picLogo.Load()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Sub New", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End
        End Try
    End Sub
    Private Sub frmUtama_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.WindowState = FormWindowState.Maximized

            ' Tentukan path gambar yang digunakan sebagai banner
            Dim bannerPath As String = String.Format("{0}\assets\banner\", Application.StartupPath())

            ' Baca file yang ada di path banner
            ' Kemudian disimpan di variable listBanner
            Dim di As New DirectoryInfo(bannerPath)
            Dim fiArr As FileInfo() = di.GetFiles()
            Dim fri As FileInfo
            For Each fri In fiArr
                listBanner.Add(fri.Name)
            Next fri

            ' Jalankan slide show banner
            If listBanner.Count > 0 Then
                SlideshowBanner()
            End If
            Timer1.Start()

            ' Ambil data dari database untuk ditampung di datatable
            M_POLITableAdapters.Fill(ds.M_POLI)
            'M_PASIENTableAdapters.Fill(ds.M_PASIEN)
            'T_KEDATANGANTableAdapters.Fill(ds.T_KEDATANGAN, Today, Now)

            refreshTime = 0
            BackgroundWorker1.RunWorkerAsync()
            Timer2.Start()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Form Load", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End Try
    End Sub

    ' Event untuk mengatur lokasi control
    ' Agar lebih dinamis
    Private Sub frmUtama_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        Try
            picClose.Left = Me.Size.Width - 80
            picScreen.Left = 369 + ((Me.Size.Width / 2) - 731)

            btnPasienBaru.Left = (Me.Size.Width / 2) - 480
            btnPasienLama.Left = (Me.Size.Width / 2) + 50
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Form Resize", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub picClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picClose.Click
        End
    End Sub
    Dim indexBanner As Integer = 0

    ' Fungsi mengatur slide banner
    Private Sub SlideshowBanner()
        Dim bannerPath As String = String.Format("{0}\assets\banner\{1}", Application.StartupPath(), listBanner(indexBanner))

        picScreen.ImageLocation = (bannerPath)
        picScreen.Load()

        ' Jika banner sudah berada pada index terakhir
        ' Maka akan dikembalikan ke index 0 lagi
        If indexBanner < listBanner.Count - 1 Then
            indexBanner += 1
        Else
            indexBanner = 0
        End If
    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Try
            SlideshowBanner()
        Catch ex As Exception
            Console.WriteLine(String.Format("Error on Timer1_Tick: {0}", ex.Message))
        End Try
    End Sub
    Private Sub btnPasienBaru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPasienBaru.Click
        Try
            ' Tampilkan form proses
            Dim frm As New frmProses(ds, "")
            frm.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Button Pasien Baru", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub btnPasienLama_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPasienLama.Click
        Try
            ' Tampilkan form scan kartu anggota
            Dim frm As New frmScan(ds)
            frm.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Button Pasien Lama", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    ' Setiap 15 detik akan meload data t kedatangan
    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        If refreshTime > 60 Then
            refreshTime = 0
            BackgroundWorker1.RunWorkerAsync()
        Else
            refreshTime += 1
        End If
    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Try
            T_KEDATANGANTableAdapters.Fill(ds.T_KEDATANGAN, Today, Now)
        Catch ex As Exception
            Console.WriteLine(String.Format("Error on BackgroundWorker1_DoWork: {0}", ex.Message))
        End Try
    End Sub
End Class
