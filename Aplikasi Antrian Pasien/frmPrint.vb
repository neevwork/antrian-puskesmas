﻿Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports Newtonsoft.Json.Linq
Imports Newtonsoft.Json

Public Class frmPrint

    ' Variable object dataset
    Dim _ds As ds

    ' Variable untuk menampung data dari form lain
    Dim _poli As String
    Dim _pasienID As String
    Dim _tipeAntrian As String
    Dim _jenisLayanan As String

    ' Variable object table adapter / query table adapter
    Dim T_KEDATANGAN_GET_STATUSTableAdapters As New dsTableAdapters.T_KEDATANGAN_GET_STATUSTableAdapter
    Dim query As New dsTableAdapters.QueriesTableAdapter

    ' Variable untuk komunikasi TCP
    Dim tl As TcpListener
    Dim ns As NetworkStream
    Dim br As BinaryReader
    Dim bw As BinaryWriter
    Dim soc As Socket
    Dim ds As New ds

    ' Counter untuk close form
    ' Setelah cetak no antrian
    Dim counter As Integer

    Sub New(ByVal poli As String, ByVal ds As ds, Optional ByVal jenisLayanan As String = Nothing, Optional ByVal tipeAntrian As String = Nothing, Optional ByVal pasienID As String = Nothing)
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Try
            ' Set logo path
            Dim logoPath As String = String.Format("{0}\assets\img\logo.png", Application.StartupPath())

            ' Tampilkan logo
            Dim bmp As New Bitmap(logoPath)
            picLogo.ImageLocation = (logoPath)
            picLogo.Height = bmp.Height
            picLogo.Width = bmp.Width
            picLogo.Load()

            ' Tampung nilai dari form lain ke variable lokal
            _ds = ds
            _poli = poli
            _pasienID = pasienID
            _tipeAntrian = tipeAntrian
            _jenisLayanan = jenisLayanan
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Sub New", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End Try
    End Sub

    ' Buat variable no atrian
    Dim NOANTRIAN As String
    Dim idPoli As String

    Private Sub UpdateMasterPoli()
        Dim dt As New DataTable
        Dim configText As String = clsPoliConfig.Load()

        ' Tampung pada object json
        Dim jsonSettings As JObject = JObject.Parse(configText.Replace(Chr(10), Chr(13)))

        Dim token As JToken = jsonSettings.GetValue("PoliConfigurations")

        dt = JsonConvert.DeserializeObject(Of DataTable)(token.ToString())

        For Each row In _ds.M_POLI
            Dim dr() As DataRow = Nothing
            Try
                dr = dt.Select(String.Format("IDPOLI='{0}'", row.IDPOLI))
            Catch ex As Exception
            End Try
            Dim display As String = ""
            Try
                display = dr(0)("DISPLAY")
            Catch ex As Exception
            End Try
            Dim prefix As String = ""
            Try
                prefix = dr(0)("PREFIX")
            Catch ex As Exception
            End Try

            row("ALIAS") = display
            row("PREFIXANTRIAN") = prefix
        Next
        _ds.M_POLI.AcceptChanges()
    End Sub

    Private Sub frmPrint_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            ' Digunakan untuk background worker
            'CheckForIllegalCrossThreadCalls = False
            'BackgroundWorker1.RunWorkerAsync()

            ' Ambil data kedatangan berdasarkan prefix masing-masing poli
            ' Dan urutkan secara descending
            ' Untuk dilanjutkan ke no antrian selanjutnya
            Dim dr As DataRow() = _ds.T_KEDATANGAN.Select(String.Format("NOANTRIAN like '{0}%'", _poli), "NOANTRIAN DESC")

            ' Cek apakah no antrian pertama
            ' Atau melanjutkan dari no antrian sebelumnya
            If dr.Length > 0 Then

                ' Jika melanjutkan
                NOANTRIAN = String.Format("{0}{1:000}", _poli, CInt(dr(0)("NOANTRIAN").ToString.Substring(1, 3)) + 1)
            Else

                ' Jika no antrian pertama
                NOANTRIAN = String.Format("{0}001", _poli)
            End If

            ' Ambil data dari database
            ' Data kedatangan pasien terkait pada hari itu
            Dim dt As DataTable = T_KEDATANGAN_GET_STATUSTableAdapters.GetData(_pasienID, Today, Now)

            ' Ambil data dari datatable
            ' Untuk poli dengan prefix terkait
            UpdateMasterPoli()
            dr = _ds.M_POLI.Select(String.Format("PREFIXANTRIAN = '{0}'", _poli))

            ' Ambil ID Poli dan Alias Polinya
            ' Untuk digunakan pada proses selanjutnya
            idPoli = Nothing
            Dim aliasPoli As String = Nothing
            If dr.Length > 0 Then
                idPoli = dr(0)("IDPOLI").ToString
                aliasPoli = dr(0)("ALIAS").ToString
            End If

            ' Cek apakah pasien baru / lama
            Dim noBPJS As String = Nothing
            Dim tipeKartu As String = "UMUM"
            If _pasienID <> "" Then

                ' Jika pasien baru maka nilai variable dr
                ' Diisi dengan data pasien terkait
                'dr = _ds.M_PASIEN.Select(String.Format("IDPASIEN = '{0}'", _pasienID))
                Dim ta As New dsTableAdapters.M_PASIENTableAdapter
                dr = ta.GetData(_pasienID).Select

                ' Jika pasien lama
                ' Maka ambil informasi bpjsnya
                If dr.Length > 0 Then
                    noBPJS = dr(0)("NOBPJSJAMKESMAS").ToString
                    tipeKartu = dr(0)("TIPEKARTU").ToString
                End If
            End If

            ' Pengecekan apakah insert antrian baru / revisi no antrian
            Dim count As Integer = 0
            Try
                count = dt.Rows.Count
            Catch ex As Exception
                count = 0
            End Try
            If count > 0 And _pasienID <> "" Then

                ' cek kembali apakah masih ada antrian yang masih belum close
                If dt.Select(String.Format("CLOSERAWATJALAN is null")).Length > 0 Then

                    ' Update data
                    If IsDBNull(dt(0)("NOANTRIAN")) Then
                        query.UpdateNoAntrianTKedatangan(idPoli, _tipeAntrian, NOANTRIAN, CInt(dt(0)("IDKEDATANGAN")))
                        NOANTRIAN = NOANTRIAN
                    Else
                        query.UpdateTKedatangan(idPoli, _tipeAntrian, CInt(dt(0)("IDKEDATANGAN")))
                        NOANTRIAN = dt(0)("NOANTRIAN")
                    End If
                Else

                    ' Insert data baru
                    query.InsertTKedatangan(Now, tipeKartu, noBPJS, 0, _pasienID, idPoli, 0, Nothing, NOANTRIAN, _tipeAntrian, 0)

                    _ds.T_KEDATANGAN.NewRow()
                    _ds.T_KEDATANGAN.Rows.Add(NOANTRIAN)
                End If
            Else
                ' insert data kedatangan baru
                query.InsertTKedatangan(Now, tipeKartu, noBPJS, 0, _pasienID, idPoli, 0, Nothing, NOANTRIAN, _tipeAntrian, 0)

                _ds.T_KEDATANGAN.NewRow()
                _ds.T_KEDATANGAN.Rows.Add(NOANTRIAN)
            End If

            '' Kirim ke client dengan format NOANTRIAN;IDPOLI;PRIORITAS
            'Try
            '    tl = New TcpListener(IPAddress.Any, CInt(My.Settings.LocalPort))
            '    tl.Start()

            '    soc = tl.AcceptSocket

            '    ns = New NetworkStream(soc)
            '    bw = New BinaryWriter(ns)

            '    bw.Write(String.Format("{0};{1};{2}", NOANTRIAN, idPoli, _tipeAntrian))

            '    soc.Close()
            '    tl.Stop()
            'Catch ex As Exception
            'End Try
            BackgroundWorker1.RunWorkerAsync()

            ' Cetak No Antrian
            Dim print As New clsPrinter
            print.PrintHeader(PotongString(My.Settings.NamaInstansi), PotongString(My.Settings.AlamatInstansi), "ANTRIAN", String.Format("{0} {1}", tipeKartu, _tipeAntrian), aliasPoli, NOANTRIAN, Now, My.Settings.PaperLen)
            print.Print()

            ' Setelah fungsi cetak jalan
            ' Maka, jalankan fungsi timer
            ' untuk close form
            counter = 1
            Timer1.Start()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Form Load", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End Try
    End Sub

    Private Function PotongString(ByVal value As String)
        If value.Length > CInt(My.Settings.PaperLen) Then
            Return value.Substring(0, CInt(My.Settings.PaperLen))
        Else
            Return value
        End If
        'Return IIf(value.Length > CInt(My.Settings.PaperLen), value.Substring(0, CInt(My.Settings.PaperLen)), value)
    End Function


    ' Event untuk mengatur lokasi control
    ' Agar lebih dinamis
    Private Sub frmUtama_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        Try
            picSuccess.Left = (Me.Size.Width / 2) - (picSuccess.Width / 2)

            Label1.Left = (Me.Size.Width / 2) - (Label1.Width / 2)
            Label2.Left = (Me.Size.Width / 2) - (Label2.Width / 2)
            btnSelesai.Left = (Me.Size.Width / 2) - (btnSelesai.Width / 2)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Form Resize", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub picClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelesai.Click
        Me.Close()
    End Sub

    ' Event close otomatis 
    ' Setelah mencetak nomor antrian
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If counter < 6 Then
            counter += 1
        Else
            Me.Close()
        End If
    End Sub

    ' Sementara tidak digunakan
    '    Private Sub BackgroundWorker1_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
    'ResetConnection:
    '        tl = New TcpListener(IPAddress.Any, CInt(My.Settings.LocalPort))
    '        tl.Start()

    '        soc = tl.AcceptSocket

    '        ns = New NetworkStream(soc)
    '        br = New BinaryReader(ns)
    '        bw = New BinaryWriter(ns)
    '        While True
    '            Try
    '                Console.WriteLine(br.ReadString)
    '            Catch ex As Exception
    '                soc.Close()
    '                tl.Stop()
    '                GoTo ResetConnection
    '            End Try
    '        End While
    '    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        ' Kirim ke client dengan format NOANTRIAN;IDPOLI;PRIORITAS
        Try
            tl = New TcpListener(IPAddress.Any, CInt(My.Settings.LocalPort))
            tl.Start()

            soc = tl.AcceptSocket

            ns = New NetworkStream(soc)
            bw = New BinaryWriter(ns)

            bw.Write(String.Format("{0};{1};{2};{3}", NOANTRIAN, idPoli, _tipeAntrian, IIf(_pasienID = "", "-", _pasienID)))

            soc.Close()
            tl.Stop()
        Catch ex As Exception
        End Try
    End Sub
End Class