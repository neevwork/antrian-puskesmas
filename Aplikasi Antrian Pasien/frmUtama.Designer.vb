﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUtama
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.picLogo = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.picBackground = New System.Windows.Forms.PictureBox()
        Me.picScreen = New System.Windows.Forms.PictureBox()
        Me.btnPasienBaru = New System.Windows.Forms.Button()
        Me.btnPasienLama = New System.Windows.Forms.Button()
        Me.picClose = New System.Windows.Forms.PictureBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBackground, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picScreen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picLogo
        '
        Me.picLogo.Dock = System.Windows.Forms.DockStyle.Top
        Me.picLogo.Location = New System.Drawing.Point(0, 0)
        Me.picLogo.Name = "picLogo"
        Me.picLogo.Size = New System.Drawing.Size(1264, 134)
        Me.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.picLogo.TabIndex = 0
        Me.picLogo.TabStop = False
        '
        'Label1
        '
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label1.Font = New System.Drawing.Font("Arial", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(0, 134)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(1264, 57)
        Me.Label1.TabIndex = 1
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label2.Font = New System.Drawing.Font("Arial", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.GrayText
        Me.Label2.Location = New System.Drawing.Point(0, 191)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(1264, 35)
        Me.Label2.TabIndex = 2
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picBackground
        '
        Me.picBackground.Dock = System.Windows.Forms.DockStyle.Top
        Me.picBackground.Image = Global.Aplikasi_Antrian_Pasien.My.Resources.Resources.background
        Me.picBackground.Location = New System.Drawing.Point(0, 226)
        Me.picBackground.Name = "picBackground"
        Me.picBackground.Size = New System.Drawing.Size(1264, 568)
        Me.picBackground.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.picBackground.TabIndex = 3
        Me.picBackground.TabStop = False
        '
        'picScreen
        '
        Me.picScreen.Location = New System.Drawing.Point(269, 272)
        Me.picScreen.Name = "picScreen"
        Me.picScreen.Size = New System.Drawing.Size(737, 475)
        Me.picScreen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picScreen.TabIndex = 4
        Me.picScreen.TabStop = False
        '
        'btnPasienBaru
        '
        Me.btnPasienBaru.BackColor = System.Drawing.Color.OrangeRed
        Me.btnPasienBaru.Font = New System.Drawing.Font("Arial", 39.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPasienBaru.ForeColor = System.Drawing.Color.White
        Me.btnPasienBaru.Location = New System.Drawing.Point(178, 810)
        Me.btnPasienBaru.Name = "btnPasienBaru"
        Me.btnPasienBaru.Size = New System.Drawing.Size(468, 153)
        Me.btnPasienBaru.TabIndex = 5
        Me.btnPasienBaru.Text = "ANTRI" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "PASIEN BARU"
        Me.btnPasienBaru.UseVisualStyleBackColor = False
        '
        'btnPasienLama
        '
        Me.btnPasienLama.BackColor = System.Drawing.Color.SeaGreen
        Me.btnPasienLama.Font = New System.Drawing.Font("Arial", 39.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPasienLama.ForeColor = System.Drawing.Color.White
        Me.btnPasienLama.Location = New System.Drawing.Point(694, 810)
        Me.btnPasienLama.Name = "btnPasienLama"
        Me.btnPasienLama.Size = New System.Drawing.Size(468, 153)
        Me.btnPasienLama.TabIndex = 6
        Me.btnPasienLama.Text = "ANTRI" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "PASIEN LAMA"
        Me.btnPasienLama.UseVisualStyleBackColor = False
        '
        'picClose
        '
        Me.picClose.Location = New System.Drawing.Point(1198, 12)
        Me.picClose.Name = "picClose"
        Me.picClose.Size = New System.Drawing.Size(54, 54)
        Me.picClose.TabIndex = 7
        Me.picClose.TabStop = False
        '
        'Timer1
        '
        Me.Timer1.Interval = 3000
        '
        'BackgroundWorker1
        '
        '
        'Timer2
        '
        Me.Timer2.Interval = 1000
        '
        'frmUtama
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1264, 985)
        Me.Controls.Add(Me.picClose)
        Me.Controls.Add(Me.btnPasienLama)
        Me.Controls.Add(Me.btnPasienBaru)
        Me.Controls.Add(Me.picScreen)
        Me.Controls.Add(Me.picBackground)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.picLogo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmUtama"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Aplikasi Antrian Pasien"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBackground, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picScreen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picLogo As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents picBackground As System.Windows.Forms.PictureBox
    Friend WithEvents picScreen As System.Windows.Forms.PictureBox
    Friend WithEvents btnPasienBaru As System.Windows.Forms.Button
    Friend WithEvents btnPasienLama As System.Windows.Forms.Button
    Friend WithEvents picClose As System.Windows.Forms.PictureBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents Timer2 As System.Windows.Forms.Timer

End Class
