﻿Public Class frmScan
    ' Variable object dataset
    Dim _ds As ds

    ' Varibale untuk menampung inputan saat scan
    Dim pasienID As String = ""

    ' Variable untuk kedipan pesan error
    Dim count As Integer = 0

    ' Variable object table adapter
    Dim T_KEDATANGAN_GET_STATUSTableAdapters As New dsTableAdapters.T_KEDATANGAN_GET_STATUSTableAdapter

    ' Variable waktu iddle aktivitas pasien
    Dim iddleTime As Integer = 0

    Sub New(ByVal ds As ds)
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Try
            ' Set logo path
            Dim logoPath As String = String.Format("{0}\assets\img\logo.png", Application.StartupPath())

            ' Tampilkan logo
            Dim bmp As New Bitmap(logoPath)
            picLogo.ImageLocation = (logoPath)
            picLogo.Height = bmp.Height
            picLogo.Width = bmp.Width
            picLogo.Load()

            ' Tampung dataset dari form utama ke dataset lokal
            _ds = ds
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Sub New", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End Try
    End Sub

    ' Event untuk mengatur lokasi control
    ' Agar lebih dinamis
    Private Sub frmUtama_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        Try
            picClose.Left = Me.Size.Width - 80
            picSuccess.Left = (Me.Size.Width / 2) - (picSuccess.Width / 2)
            Label1.Left = (Me.Size.Width / 2) - (Label1.Width / 2)
            Label2.Left = (Me.Size.Width / 2) - (Label2.Width / 2)
            lblError.Left = (Me.Size.Width / 2) - (lblError.Width / 2)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Form Resize", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub picClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picClose.Click
        Me.Close()
    End Sub

    ' Event untuk menghandle fungsi scan 
    ' Dari barcode scanner
    Private Sub frmScan_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If (e.KeyChar = vbCr) Then
                ' Jika inputan adalah enter maka
                ' Cari data pasien berdasarkan hasil scan
                'Dim dr As DataRow() = _ds.M_PASIEN.Select(String.Format("IDPASIEN = '{0}'", pasienID))
                Dim ta As New dsTableAdapters.M_PASIENTableAdapter
                Dim dr As DataRow() = ta.GetData(pasienID).Select

                ' Pengecekan apakah pasien sudah terdaftar
                If dr.Length > 0 Then
                    ' Cek status kedatangan pasien di database
                    Dim dt = T_KEDATANGAN_GET_STATUSTableAdapters.GetData(pasienID, Today, Now)

                    ' Pengecekan rawat jalan / rawat inap
                    If dt.Rows.Count = 0 Then

                        ' Jika rawat jalan
                        Me.Close()
                        Dim frm As New frmSambutan(_ds, pasienID)
                        frm.ShowDialog()
                    Else

                        ' Jika data ada
                        If dt.Rows(0)("STATUSRAWATINAP") = 0 Then

                            ' Jika rawat jalan
                            Me.Close()
                            Dim frm As New frmSambutan(_ds, pasienID)
                            frm.ShowDialog()
                        Else

                            ' Jika rawat inap
                            count = 0
                            lblError.Text = "HANYA UNTUK RAWAT JALAN"
                            lblError.Visible = True
                            Timer1.Start()
                        End If
                    End If
                Else
                    ' Jika data tidak ditemukan
                    count = 0
                    lblError.Text = "DATA ANDA TIDAK TERDAFTAR"
                    lblError.Visible = True
                    Timer1.Start()
                End If

                ' Jika proses pengecekan sudah dilakukan
                ' Maka kosongkan kembali variable pasienID
                pasienID = ""
            Else
                ' Tampung setiap karakter yang discan
                pasienID = pasienID + e.KeyChar
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error on Form Keypress", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    ' Timer untuk menampilkan pesan error
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If count < 7 Then
            If lblError.Visible Then
                lblError.Visible = False
            Else
                lblError.Visible = True
            End If
            count += 1
        Else
            Timer1.Stop()
        End If
    End Sub
    ' Event untuk menutup form
    ' Jika user tidak melakukan apa-apa
    ' Selama 30 detik
    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        If iddleTime > 30 Then
            Me.Close()
        Else
            iddleTime += 1
        End If
    End Sub

    Private Sub frmScan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Atur timer 30 detik otomatis close form
        ' Jika pasien tidak melakukan apa-apa
        iddleTime = 0
        Timer2.Start()
    End Sub
End Class